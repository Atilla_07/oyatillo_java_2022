import java.util.ArrayList;

public class IntersectionOfArray {

    public static void solution(int[] nums1, int[] nums2){
        int len1 = nums1.length;
        int len2 = nums2.length;
        int index = 1000;

        HeapSort.check(len1,1,index);
        HeapSort.check(len2,1,index);
        HeapSort.check(nums1,0,index);
        HeapSort.sort(nums1);
        HeapSort.check(nums2,0,index);
        HeapSort.sort(nums2);
        ArrayList<Object> result = new ArrayList<>();
        int i = 0 ;
        int j = 0;
        while(i < len1 && j< len2) {
            if (nums1[i] > nums2[j]) {
                j++;
            } else if (nums1[i] < nums2[j]){
                i++;

            } else {
                result.add(nums1[i]);
                i++;
                j++;
            }
        }
        System.out.println("Ваш результат: " + result);
    }

}
