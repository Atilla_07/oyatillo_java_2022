public class Bracket {

    public static void check(char[] array, char br1, char br2){
        for (int i = 0; i < array.length; i++) {
            if (array[i] == br1) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] == br2) {
                        array[i] = '+';
                        array[j] = '+';
                        break;
                    }
                }
            }
        }
    }

    public static boolean solution(String bracket) {
        char[] array = bracket.toCharArray();

        if (array.length % 2 != 0)
            return false;

        check(array, '(', ')');
        check(array, '[', ']');
        check(array, '{', '}');


        for (char c : array) {
            if (c != '+')
                return false;
        }
        return true;
    }
}

