import java.util.Arrays;

public class RotateArray {

    public static void solution(int[] nums, int k){
        int x = 0;
        int len = nums.length;
        int index = Integer.MAX_VALUE / 2;
        int indexLen = 100000;
        int[] temp = new int[]{};

        if((0 <= k) && (k <= indexLen)) {
            temp = new int[k];
        }

        HeapSort.check(len,1,indexLen);
        HeapSort.check(temp,0,indexLen);
        HeapSort.check(nums,-index,index);
        for (int i = 0; i < len; i++) {
            if (i >= len - k){
                temp[x] = nums[i];
                nums[i] = 0;
                x++;
            }
        }

        int[] readyArr = new int[len];
        x = k;
        for (int i = 0; i < len; i++) {
            if (k > i){
                readyArr[i] = temp[i];
            }
            if (k <= i) {
                readyArr[i] = nums[i-x];
            }
        }
        System.out.println("Ваш результат: " + Arrays.toString(readyArr));
    }

}

//1073741824 -> 2^31
//2147483648 -> 2^32
//2147483647 -> Integer.MAX_VALUE
//1073741823 -> Integer.MAX_VALUE / 2 = 2^31 - 1
//-1073741824 -> -(Integer.MAX_VALUE / 2 + 1) = -2^31
//-2^31 <= nums[i] <= 2^31 - 1