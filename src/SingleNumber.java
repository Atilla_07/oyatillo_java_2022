public class SingleNumber {

    public static void solution(int[] nums){
        int len = nums.length;
        int index = 30000;

        HeapSort.check(len,1,index);
        HeapSort.check(nums,-index,index);
        HeapSort.sort(nums);
        for (int i = 0; i < len; i = i+2) {
            try {
                if (nums[i] != nums[i+1]) {
                    System.out.println("Ваш результат: " + nums[i]);
                    break;
                }
            }
            catch (ArrayIndexOutOfBoundsException e){
                System.out.println("Ваш результат: " + nums[len-1]);
            }
        }
    }

}