public class Test {

    public static void main(String[] args) {

        // SingleNumber
        int[] arr1 = {2,2,1};
        int[] arr2 = {4,1,2,1,2};
        int[] arr3 = {1};
        SingleNumber.solution(arr1);
        SingleNumber.solution(arr2);
        SingleNumber.solution(arr3);
        System.out.println();

        // RotateArray
        int[] arr4 = {1,2,3,4,5,6,7};
        int[] arr5 = {-1,-100,3,99};
        RotateArray.solution(arr4,3);
        RotateArray.solution(arr5,2);
        System.out.println();

        // IntersectionOfArray
        int [] arr6 = {1,2,2,1}; int [] arr7 = {2,2};
        int [] arr8 = {4,9,5}; int [] arr9 = {9,4,9,8,4};
        IntersectionOfArray.solution(arr6,arr7);
        IntersectionOfArray.solution(arr8,arr9);
        System.out.println();

        // Permutation
        int[] arr10 = {1, 2, 3};
        int[] arr11 = {0, 1};
        int[] arr12 = {1};
        Permutation.solution(arr10);
        Permutation.solution(arr11);
        Permutation.solution(arr12);
        System.out.println();

        // InsertPosition
        int[] arr13 = {1,3,5,6};
        InsertPosition.solution(arr13,5);
        InsertPosition.solution(arr13,2);
        InsertPosition.solution(arr13,7);
        System.out.println();

        // SellStock
        int[] arr14 = {7,1,5,3,6,4};
        int[] arr15 = {7,6,4,3,1};
        SellStock.solution(arr14);
        SellStock.solution(arr15);
        System.out.println();

        // Bracket
        String str1 = "()";
        String str2 = "()[]{}";
        String str3 = "(]";
        System.out.println(Bracket.solution(str1));
        System.out.println(Bracket.solution(str2));
        System.out.println(Bracket.solution(str3));

    }

}
