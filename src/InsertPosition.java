public class InsertPosition {

    public static void solution(int[] nums, int target){
        int len = nums.length;
        int index = 10000;
        int k = 0;

        HeapSort.check(target,-index,index);
        HeapSort.check(len,1,index);
        HeapSort.check(nums,-index,index);
        for (int i = 0; i < len; i++) {
            if (nums[i] < target){
                k++;
            }
            if (nums[i] == target){
                k = i;
                break;
            }
        }
        System.out.println("Ваш результат: " + k);
    }

}
