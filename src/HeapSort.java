public class HeapSort {

    public static void check(int[] arr, int index1, int index2){
        for (int i = 0; i < arr.length; i++) {
            if (!(index1 <= arr[i]) || !(arr[i] <= index2)){
                throw new IndexOutOfBoundsException();
            }
        }
    }

    public static void check(int len, int index1, int index2){
        if (!((index1 <= len) && (len <= index2))){
            throw new IndexOutOfBoundsException();
        }
    }

    public static void sort(int[] arr) {
        int len = arr.length;

        for (int i = len / 2 - 1; i >= 0; i--)
            heapify(arr, len, i);

        for (int i=len-1; i>=0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, i, 0);
        }
    }

    public static void heapify(int[] arr, int n, int i) {
        int largest = i;
        int left = 2*i + 1;
        int right = 2*i + 2;

        if (left < n && arr[left] > arr[largest])
            largest = left;

        if (right < n && arr[right] > arr[largest])
            largest = right;

        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            heapify(arr, n, largest);
        }
    }

}
