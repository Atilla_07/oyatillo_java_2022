public class SellStock {

    public static int maxPrice(int[] different){
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < different.length; i++) {
            if (different[i] == 0){
                for (int j = i; j < different.length; j++) {
                    if (max < different[j]){
                        max = different[j];
                    }
                }
                return max;
            }
        }
        return 0;
    }

    public static void solution(int[] nums){
        int len = nums.length;
        int index = 100000;
        int price = 0;
        int[] different = new int[len];
        System.arraycopy(nums, 0, different, 0, len);

        HeapSort.check(len,1,index);
        HeapSort.check(nums,0,index / 10);
        for (int num : nums) {
            for (int j = 0; j < len; j++) {
                different[j] -= num;
            }
            if (price < maxPrice(different)) {
                price = maxPrice(different);
            }
            System.arraycopy(nums, 0, different, 0, len);
        }
        System.out.println("Ваш результат: " + price);
    }

}

// [7,1,5,3,6,4] [0,-6,-2,-4,-1,-3] - NO
// [7,1,5,3,6,4] [6,0,4,2,5,3] - 0->5
// [7,1,5,3,6,4] [2,-4,0,-2,1,-1] 0->1
// [7,1,5,3,6,4] [4,-2,2,0,3,1] 0->3
// [7,1,5,3,6,4] [1,-5,-1,-3,0,-2] NO
// [7,1,5,3,6,4] [3,-3,1,-1,2,0] NO